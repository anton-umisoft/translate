<?php

$translate = array(
	'order_form_success' => 'Gracias por su interes. Nos pondremos en contacto con usted lo más pronto posible.',
	'order_form_title' => 'Formulario de contacto',
	'order_name_title' => 'Su nombre:',
	'order_phone_title' => 'Teléfono:',
	'order_message_title' => 'Mensaje:',
);

$dir = __DIR__ . "/../../data/host/$options[name]/root";
echo "Set current dir to $dir\n";
chdir($dir); // <---------------- MAGIC STARTS HERE 


$objects = new selector('objects');
$objects->limit(0, 1);
$objects->types('object-type')->guid('umiruguid-type-author-info');

if ($objects->length() > 0) {

	$obj = array_shift($objects->result());
	if ($obj instanceof umiObject) {

		foreach ($translate as $id => $to) {
			$value = $obj->getValue($id);

			if ($value !== null && $value !== $to) {
				echo "Translate ($id) varchar " . $style("\033[0;41m") . $value . $style("\033[0m") . "\n";
				echo "with " . $style("\033[0;42m") . $to . $style("\033[0m") . "\n";
				
				$obj->setValue($id, $to);
			}
		}

		$obj->commit();
	}

} else {
	echo "umiruguid-type-author-info has not found.";
}
